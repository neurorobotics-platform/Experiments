import time

from model_definition import *
from yaca_support import Perf


class ModuleMotor:

    def __init__(self, state):

        self.state = state
        self.perf = Perf("ModuleMotor")

        # cmd vel gui state
        self.cmd_vel_gui = None

        # init components
        self.wpg_init()

    def perf(self):

        # performance test
        ti = time.time()
        for i in range(0, 50):
            self.tick()
        perf_report("ModuleMotor", ti)

    def tick(self):

        # performance
        self.perf.tick_i()

        # run components
        self.mpg()
        self.wpg()

        # performance
        self.perf.tick_f()

    def mpg(self):

        # get position of fovea in BODY
        fovea_BODY = self.state.kc_m.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_BODY,
                                                    self.state.pars.fovea_HEAD)

        # virtual floor is implemented as an internal push, here
        intrusion_through_floor_z = np.maximum(self.state.pars.virtual_floor_z - fovea_BODY[2], 0.0)
        pushvec_BODY = np.array([0.0, 0.0, intrusion_through_floor_z])
        pushvec_HEAD = self.state.kc_m.changeFrameRel(KC_FRAME_BODY, KC_FRAME_HEAD, pushvec_BODY)
        push = KinematicPush('virtfloor')
        push.flags = KC_PUSH_FLAG_IMPULSE
        push.pos = self.state.pars.fovea_HEAD
        push.vec = pushvec_HEAD

        # only actually apply the virtfloor push if it's significant, which helps with debugging,
        # because it means that once we (more or less) come above the virtfloor we stop seeing pushes
        if np.linalg.norm(push.vec) > 1e-6:
            self.state.sig.pushes.append(push)
            # print "virtfloor", fovea_BODY[2], np.linalg.norm(push.vec), push.vec, pushvec_BODY

        # virtual ceiling is implemented as an internal push, here
        intrusion_through_ceiling_z = np.maximum(fovea_BODY[2] - self.state.pars.virtual_ceiling_z,
                                                 0.0)
        pushvec_BODY = np.array([0.0, 0.0, -intrusion_through_ceiling_z])
        pushvec_HEAD = self.state.kc_m.changeFrameRel(KC_FRAME_BODY, KC_FRAME_HEAD, pushvec_BODY)
        push = KinematicPush('virtceiling')
        push.flags = KC_PUSH_FLAG_IMPULSE
        push.pos = self.state.pars.fovea_HEAD
        push.vec = pushvec_HEAD

        # only actually apply the virtceiling push if it's significant, which helps with debugging,
        # because it means that once we (more or less) come above the virtceiling we stop seeing pushes
        if np.linalg.norm(push.vec) > 1e-6:
            self.state.sig.pushes.append(push)

        # show pushes?
        show_pushes = False

        # show pushes
        if show_pushes:
            print("---- pushes ----")

        # for each external push
        for push in self.state.sig.pushes:

            # apply push to kc
            self.state.kc_m.push(push)

            # show pushes
            if show_pushes:
                print(push.name, push.vec)

        # zero kc_m pose if there are no pushes happening - is
        # this necessary? I think we only ever did it to head
        # off any long-term build up of precision error in pose
        # which is not really realistic...
        # if len(self.state.sig.pushes) == 0:
        #	self.state.kc_m.zeroPose()

        # clear pushes
        self.state.sig.pushes = []

        # write neck_cmd
        config = self.state.kc_m.getConfig()
        self.state.sig.neck_cmd = np.array(config)

        # write cmd_vel
        dpose = self.state.kc_m.getPoseChange()  # this call zeroes accumulated pose change in kc_m, also
        self.state.sig.cmd_vel[0] = dpose[0] * self.state.pars.fS_lo
        self.state.sig.cmd_vel[1] = dpose[1] * self.state.pars.fS_lo
        self.state.sig.cmd_vel[2] = dpose[2] * self.state.pars.fS_lo

        # debug pose
        # print "kc_m pose", self.state.kc_m.getPose()

        # receive gui command velocity
        if self.state.sig.input.cmd_vel_gui is not None:
            self.cmd_vel_gui = [self.state.sig.input.cmd_vel_gui, 10]

        # action gui command velocity
        if self.cmd_vel_gui is not None:
            self.state.sig.cmd_vel += self.cmd_vel_gui[0]
            self.cmd_vel_gui[1] -= 1
            if self.cmd_vel_gui[1] == 0:
                self.cmd_vel_gui = None

    def wpg_init(self):

        self.wpg_T = 1.0 / self.state.pars.fW
        self.wpg_t = 0.0
        self.wpg_macon = self.state.sig.macon_lo
        self.t_base = 0.0
        self.x = 0.0
        # self.protracting = True
        self.orienting = False

    def wpg(self):

        # if orienting, rebase time to end of orient
        if self.state.sig.orienting:
            self.t_base = self.state.time_t

        # if ending an orient, clear macon
        if self.orienting and not self.state.sig.orienting:
            self.wpg_macon.fill(0.0)

        # mark if orienting
        self.orienting = self.state.sig.orienting

        # get rebased time
        t_rebased = self.state.time_t - self.t_base

        # get periodic rebased time
        t_periodic = np.mod(t_rebased, self.wpg_T)

        # reset macon at start of new whisk, whether
        # we oriented or not
        if t_periodic < self.wpg_t:
            self.wpg_macon.fill(0.0)

        # record last periodic time
        self.wpg_t = t_periodic

        # protracting?
        q = t_periodic / self.wpg_T
        protracting = False
        if q > 0.0 and q <= 0.5:
            protracting = True

        # peak protraction
        """
        if protracting != self.protracting:
            self.protracting = protracting
            if protracting == False:
                # moment of peak protraction
                self.state.sig.peak_protraction = 1
        """

        # we no longer emit peak protraction, rather
        # the raw protracting signal
        self.state.sig.protracting = protracting

        # max with current contact signal
        c = self.state.sig.macon_lo
        self.wpg_macon = np.maximum(self.wpg_macon, c)

        # nominal whisking parameters
        theta_rcp = self.state.pars.theta_rcp
        theta_min = self.state.pars.theta_min
        theta_max = self.state.pars.theta_max
        theta_spread = self.state.pars.theta_spread

        # compute whisking ranges
        theta_rng = theta_max - theta_min
        theta_rng2 = theta_rcp - theta_max

        # get periodic motion
        x = np.cos(t_periodic * self.state.pars.fW * 2 * np.pi) * -0.5 + 0.5

        # filter it so it doesn't change too rapidly in case of rapid retraction
        self.x += 0.1 * (x - self.x)
        x = self.x

        # get theta target for each whisker
        theta_cmd = theta_min + x * theta_rng

        # HACK disable RCP
        # self.wpg_macon *= 0.0

        # implement RCP (and generate signal for all whiskers)
        theta_lim = theta_max + self.wpg_macon * theta_rng2

        # change RCP to be per-row rather than per-whisker
        theta_lim = np.repeat(np.amin(theta_lim, axis=1)[:, np.newaxis], 4, axis=1)

        # limit protraction by RCP
        theta_cmd = np.minimum(theta_cmd, theta_lim)

        # add a little spread
        spread = theta_spread * np.array([0.0, -1.0, -2.0, -3.0])
        spread = np.repeat(spread[np.newaxis, :], 6, axis=0)
        theta_cmd += spread

        # store
        self.state.sig.theta_cmd = theta_cmd
