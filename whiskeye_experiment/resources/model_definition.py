from kc_interf import *


class Focus:

    def __init__(self):
        self.pos = np.array([0.0, 0.0, 0.0])
        self.mag = np.array(0.0)
        self.max = np.array(0.0)


class SystemSig:

    def __init__(self, pars):
        # zero input
        z = np.zeros(pars.shape_hi)
        z_2 = np.zeros(pars.shape_hi_2)
        z_lo = np.zeros(pars.shape_lo)

        # platform output is model input
        self.input = None

        # filtered inputs
        self.theta_f = z.copy()
        self.xy_q = z_2.copy()
        self.xy_f = z_2.copy()
        self.macon = z.copy()

        # downsampled inputs
        self.macon_lo = z_lo.copy()
        self.theta_lo = z_lo.copy()

        # run-time whisker tip positions
        sz = pars.shape_lo[:]
        sz.append(3)
        self.whisker_tip_pos = np.zeros(sz)

        # spatial
        self.focus = Focus()
        self.contacts = []

        # world
        self.visited_latest_HEAD = None
        self.visited_list_WORLD = []
        self.contacted_list_WORLD = []
        self.head_center_WORLD = None

        # action
        self.pushes = []
        self.orienting = False

        # mode output is platform input
        self.neck_cmd = np.array([WHISKY_LIFT_INI_RAD, WHISKY_PITCH_INI_RAD, WHISKY_YAW_INI_RAD])
        self.theta_cmd = np.zeros(pars.shape_lo)
        # self.peak_protraction = 0
        self.protracting = False
        self.cmd_vel = np.array([0.0, 0.0, 0.0])

        # internals outputs for debugging
        self.mapval = None
        self.ior = None
        self.sausage = None
        self.sausage_squeezed = None


class SystemState:

    def __init__(self):

        self.pars = SystemPars()
        self.sig = SystemSig(self.pars)
        self.id = 0

        # trace file
        self.tracefile = None

        # time
        self.time_n = 0
        self.time_t = 0.0
        self.time_report_T = 10.0
        self.time_report = self.time_report_T - 0.01

        # kinematic chains
        self.kc_s = kc_whiskeye()
        self.kc_m = kc_whiskeye()
        self.kc_w = kc_whiskers(self.pars)

        # procedural
        self.cancel = False

        # open log file
        self.logfile = open('/tmp/yacalog', 'w')

    def tick(self):

        # update time
        self.time_n += 1
        self.time_t += 0.02

        # report time
        if self.time_t >= self.time_report:
            print("\n****************",
                  "\nrun time in seconds", self.time_t,
                  "\n****************", "\n")
            self.time_report += self.time_report_T

    def logwriteitem(self, x):

        if isinstance(x, str):
            self.logfile.write(x)

        else:
            self.logfile.write(str(x))

    def logwrite(self, x, tag=None):

        if tag is not None:
            self.logfile.write(tag + " ")

        if type(x) is list or type(x) is tuple:
            for q in x:
                self.logwriteitem(q)
                self.logwriteitem(" ")
        else:
            self.logwriteitem(x)

        self.logfile.write("\n")

    def get_id(self):

        self.id += 1
        return self.id
