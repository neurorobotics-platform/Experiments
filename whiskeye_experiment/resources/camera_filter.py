#!/usr/bin/python

# import ROS
import rospy
import std_msgs
from sensor_msgs.msg import CompressedImage

# global
g_cam0 = None
g_cam1 = None
g_a = 0
g_n = 0


# callback
def callback_protracting(a):
    a = a.data
    global g_a, g_n, g_cam0, g_cam1
    if a == 0 and g_a == 1:
        g_n = g_n + 1
        print("peak_protraction", g_n)
        if g_cam0 is None:
            print("**** **** cam0 not available yet **** ****")
        else:
            pub_cam0.publish(g_cam0)
        if g_cam1 is None:
            print("**** **** cam1 not available yet **** ****")
        else:
            pub_cam1.publish(g_cam1)
    g_a = a


# callback
def callback_cam0(msg):
    global g_cam0
    g_cam0 = msg


# callback
def callback_cam1(msg):
    global g_cam1
    g_cam1 = msg


# init node
# rospy.init_node('camera_filter')

# publish
topic = '/model/cam0/compressed'
print("publishing", topic)
pub_cam0 = rospy.Publisher(topic, CompressedImage, queue_size=0)

# publish
topic = '/model/cam1/compressed'
print("publishing", topic)
pub_cam1 = rospy.Publisher(topic, CompressedImage, queue_size=0)

# subscribe
topic = '/model/protracting'
print("subscribing", topic)
sub_protracting = rospy.Subscriber(topic, std_msgs.msg.Int8, callback_protracting, queue_size=1,
                                   tcp_nodelay=True)

# subscribe
topic = '/whiskeye/head/cam0/image_raw/compressed'
print("subscribing", topic)
sub_cam0 = rospy.Subscriber(topic, CompressedImage, callback_cam0, queue_size=1, tcp_nodelay=True)

# subscribe
topic = '/whiskeye/head/cam1/image_raw/compressed'
print("subscribing", topic)
sub_cam1 = rospy.Subscriber(topic, CompressedImage, callback_cam1, queue_size=1, tcp_nodelay=True)

# loop
# while True:
#    if rospy.is_shutdown():
#        raise ValueError("ROS shutdown")
#    time.sleep(0.001)
