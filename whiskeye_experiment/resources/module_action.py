import time

from kc_interf import *
from yaca_support import Perf

TEST_ORIENT = False


class ActionClock(object):

    def __init__(self):

        self.steps_so_far = 0
        self.steps_total = 0
        self.T_norm = 0.0
        self.t_norm = 0.0

    def start(self, steps_total):

        # start integral clock
        self.steps_total = steps_total
        self.steps_so_far = 0

        # start normalised clock
        self.T_norm = 1.0 / steps_total
        self.t_norm = self.T_norm * 0.5

    def toString(self):

        return str(self.steps_so_far) + "/" + str(self.steps_total) + " (" + str(self.t_norm) + ")"

    def advance(self, auto_stop=False):

        # advance
        self.steps_so_far += 1
        self.t_norm += self.T_norm

        # auto stop at complete
        #
        # NB: both integral and norm clocks can overrun the expected time of
        # pattern safely; some patterns wait for another termination condition
        if auto_stop:
            if self.steps_so_far >= self.steps_total:
                self.stop()

    def stop(self):

        # reset clock
        self.steps_total = 0
        self.steps_so_far = 0
        self.T_norm = 0
        self.t_norm = 0

    def isActive(self):

        return self.steps_total > 0

    def isRunning(self):

        return self.steps_total > 0 and self.steps_so_far > 0

    def cosine_profile(self):

        # return a profile from 0 to 1 (for t > steps_total, return 1)
        if self.t_norm >= 1.0:
            return 1.0

        return 0.5 - 0.5 * np.cos(self.t_norm * np.pi)

    def pulse_cosine_profile(self):

        # return a profile that goes from 0 to 1 and back to 0 (for t > steps_total, return 0)
        if self.t_norm >= 1.0:
            return 0.0

        return 0.5 - 0.5 * np.cos(self.t_norm * np.pi * 2.0)

    def linear_profile(self):

        # Returns a profile from 0 to 1, for t > steps_total, return 1
        if self.t_norm >= 1.0:
            return 1.0

        return self.t_norm


class ActionTemplate(object):

    def __init__(self, state, name):
        self.clock = ActionClock()
        self.state = state
        self.t_start = None
        self.name = name

        # all actions have a KC that they may use for model-based control
        self.kc_g = kc_whiskeye()


class ActionMull(ActionTemplate):

    def tick(self, inh):

        # handle start/stop
        if self.t_start is None:
            if inh == 0.0:
                self.start()
        else:
            if inh > 0.0:
                self.stop()

        # default (null) push
        push = None  # KinematicPush('mull')

        # if active
        if self.t_start is not None:

            # priority for ongoing action
            pri = self.state.pars.pri_idle

        else:

            # priority at idle
            pri = self.state.pars.pri_idle

        # ok
        return (pri, push)

    def start(self):

        # store start time
        self.t_start = self.state.time_t

    def stop(self):

        # clear start time
        self.t_start = None


class ActionOrient(ActionTemplate):

    def tick(self, inh):

        # handle start/stop
        if not self.clock.isActive():
            if inh == 0.0:
                self.start()
        else:
            if inh > 0.0:
                self.stop()

        # default (null) push
        push = None

        # get priority of focus centroid magnitude
        pri_focus = self.state.sig.focus.max.clip(max=1.0)

        # if active
        if self.clock.isActive():

            # orienting
            self.state.sig.orienting = True

            # read clock
            q = self.clock.cosine_profile()
            u = self.clock.pulse_cosine_profile()
            running = self.clock.isRunning()
            self.clock.advance(False)

            # if action finished
            if q >= 1.0:

                # bid with zero priority to be deselected
                pri = 0.0

            # if action ongoing
            else:

                # compute target in WORLD
                x_WORLD = self.action_i_WORLD + q * self.daction_WORLD + u * self.shimmy_WORLD

                # get kc
                kc = self.kc_g
                kc.debug = True

                # compute target in HEAD
                x_HEAD = kc.changeFrameAbs(
                    KC_FRAME_PARENT,
                    KC_FRAME_HEAD,
                    x_WORLD)

                # send push
                push = KinematicPush('orient')
                push.flags = KC_PUSH_FLAG_IMPULSE
                push.pos = self.state.pars.fovea_HEAD
                push.vec = x_HEAD - push.pos

                # debug
                if False:
                    s = kc.getState()
                    self.state.logwrite((q, np.linalg.norm(push.vec), \
                                         x_WORLD[0], x_WORLD[1], x_WORLD[2], \
                                         x_HEAD[0], x_HEAD[1], x_HEAD[2], \
                                         s[0][0][0], s[0][0][1], s[0][1], s[1][0], s[1][1],
                                         s[1][2]))

                # debug
                if False:
                    f_WORLD = kc.changeFrameAbs(
                        KC_FRAME_HEAD,
                        KC_FRAME_PARENT,
                        self.state.pars.fovea_HEAD)
                    pv = kc.changeFrameRel(
                        KC_FRAME_HEAD,
                        KC_FRAME_PARENT,
                        push.vec)
                    print(q, x_WORLD, f_WORLD, np.linalg.norm(pv[0:2]))

                # priority for ongoing action
                pri = self.state.pars.pri_ongoing

                # end action if stimulus is encountered
                if running and pri_focus > self.state.pars.thresh_cancel:
                    # bid with zero priority to be deselected
                    pri = 0.0

        else:

            # kc_g must track config so it is correctly organised
            # when an orient begins (but during an orient it may
            # become de-synced with kc_m and/or kc_s, since it is
            # only implementing open-loop control). we track kc_m
            # because kc_s is effectively servoed to kc_m, and if
            # we track kc_s then we may actually drift slightly
            # outside the angle constraints leading to impulses
            # in the drive signal when an orient begins (as those
            # constraints are restored).
            # self.kc_g.setConfig(self.state.sig.input.neck) # do not track kc_s
            self.kc_g.setConfig(self.state.kc_m.getConfig())  # track kc_m

            # priority is based on focus centroid magnitude
            pri = pri_focus

            # test orient?
            global TEST_ORIENT
            if TEST_ORIENT:

                # raise priority at exactly one point
                pri = 0.0
                if self.state.time_n == 10:
                    print("TEST_ORIENT start")
                    pri = 1.0

                # and exit after completion
                if self.state.time_n > 50:
                    print("TEST_ORIENT exit")
                    exit()

        # if pushed, apply that to kc_g to do closed-loop control
        if push is not None:
            self.kc_g.push(push)

        # ok
        return (pri, push)

    def start(self):

        # get kc
        kc = self.kc_g

        # zero kc_g pose before we start, only if it helps
        # with debugging - it sometimes helps with debugging
        # if we /don't/ do this!
        kc.zeroPose()

        # get target
        tgt_HEAD = self.state.sig.focus.pos

        # test orient?
        global TEST_ORIENT
        if TEST_ORIENT:
            tgt_HEAD = np.array([-0.5, 0.5, 0.0]) + self.state.pars.fovea_HEAD

        # get start point in WORLD
        self.action_i_WORLD = kc.changeFrameAbs(
            KC_FRAME_HEAD,
            KC_FRAME_PARENT,
            self.state.pars.fovea_HEAD)

        # get end point in WORLD
        self.action_f_WORLD = kc.changeFrameAbs(
            KC_FRAME_HEAD,
            KC_FRAME_PARENT,
            tgt_HEAD)

        # store
        self.state.sig.visited_latest_HEAD = copy.copy(tgt_HEAD)

        # compute required foveal vector
        self.daction_WORLD = self.action_f_WORLD - self.action_i_WORLD

        # compute required shimmy vector
        shimmy_HEAD = np.array([-self.state.pars.orient_shimmy_dist, 0.0, 0.0])
        self.shimmy_WORLD = kc.changeFrameRel(
            KC_FRAME_HEAD,
            KC_FRAME_PARENT,
            shimmy_HEAD)

        # compute orient distance
        dist = np.sqrt((self.daction_WORLD * self.daction_WORLD).sum())

        # compute orient time
        t_orient = np.maximum(dist / self.state.pars.orient_max_speed,
                              self.state.pars.orient_min_time)
        n_orient = int(t_orient * self.state.pars.fS_lo)

        # report
        if self.state.pars.debug_actions:
            print("**** orient ****")
            print("dist = {:.3f}".format(dist), "time = {:.3f}".format(t_orient))
            print("tgt_i_WORLD", self.action_i_WORLD)
            print("tgt_f_WORLD", self.action_f_WORLD)
            # print "tgt_i_BODY", kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_BODY, self.state.pars.fovea_HEAD)
            # print "tgt_f_BODY", kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_BODY, tgt_HEAD)
            print("********")

        # start clock
        self.clock.start(n_orient)

    def stop(self):

        # clear start time
        self.clock.stop()


class BasalGanglia:

    def __init__(self, state, nchan):
        self.state = state
        self.inh = np.ones(nchan)

    def tick(self, pri):
        i_bak = np.argmin(self.inh)
        i = np.argmax(pri)
        self.inh = np.ones(len(self.inh))
        self.inh[i] = 0.0

        # report
        return (i, i_bak)


class ModuleAction:

    def __init__(self, state):

        self.state = state
        self.perf = Perf("ModuleAction")

        # init actions
        self.actions = (ActionMull(state, "mull"), ActionOrient(state, "orient"))
        # self.actions = [ActionMull(state, "mull")]
        # print("\n\n******************** NO ORIENT \n\n")

        # init selection
        self.bg = BasalGanglia(state, len(self.actions))

    def perf(self):

        # performance test
        ti = time.time()
        for i in range(0, 50):
            self.tick()
        perf_report("ModuleAction", ti)

    def tick(self):

        # performance
        self.perf.tick_i()

        # not orienting
        self.state.sig.orienting = False

        # get nchan
        nchan = len(self.actions)

        # collate input
        pris = np.zeros(nchan)

        # get bg output
        inh = self.bg.inh

        # run actions
        for i in range(0, len(self.actions)):
            [pris[i], push] = self.actions[i].tick(inh[i])

            # continue

            if push is not None:
                self.state.sig.pushes.append(push)

        # run selection
        sel = self.bg.tick(pris)

        # report
        if self.state.pars.debug_actions:
            i = sel[0]
            i_bak = sel[1]
            if not i == i_bak:
                print(self.actions[i_bak].name, "->", self.actions[i].name)

        # performance
        self.perf.tick_f()
