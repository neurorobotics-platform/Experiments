from kc_interf import *
from yaca_support import *


class ModuleWorld:

    def __init__(self, state):

        self.state = state
        self.perf = Perf("ModuleWorld")

        # state
        self.visited_list_t = []
        self.contacts = np.zeros((0, 6))

    def tick(self):

        # performance
        self.perf.tick_i()

        # get sensory kc
        kc = self.state.kc_s

        # get head center in WORLD
        self.state.sig.head_center_WORLD = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT,
                                                             self.state.pars.center_HEAD)

        # add new visited location, if available
        if self.state.sig.visited_latest_HEAD is not None:
            # get and clear
            visited_WORLD = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT,
                                              self.state.sig.visited_latest_HEAD)
            self.state.sig.visited_latest_HEAD = None

            # add to list
            self.state.sig.visited_list_WORLD.append(visited_WORLD)
            self.visited_list_t.append(self.state.time_t)

        # retire old visited locations
        if len(self.visited_list_t) > 0:

            # get age of oldest
            t = self.visited_list_t[0]
            age = self.state.time_t - t

            # if that age is too old, remove it
            if age > self.state.pars.ior_max_age:
                self.visited_list_t = self.visited_list_t[1:]
                self.state.sig.visited_list_WORLD = self.state.sig.visited_list_WORLD[1:]

        # retire old contacts
        if len(self.contacts) > 0:

            # get age of oldest
            oldest = self.contacts[0]
            t = oldest[4]
            age = self.state.time_t - t

            # if that age is too old, remove it
            if age > self.state.pars.contacts_max_age:
                self.contacts = self.contacts[1:]

        # add contacts
        contacts = self.state.sig.contacts
        if len(contacts) > 0:

            # clear incoming contacts
            self.state.sig.contacts = []

            # for each incoming
            for contact in contacts:

                # extract
                pos = contact[0]
                w = contact[1]

                # compare pos with stored pos
                if len(self.contacts) > 0:

                    # find geometrically-closest stored contact (in x/y)
                    x = self.contacts[:, :2] - pos[:2]
                    d = np.sum(x ** 2, 1)
                    a = np.argmin(d)  # index of nearest stored contact
                    d = np.sqrt(d[a])  # its distance from the current contact

                    # if it's close enough
                    if d < self.state.pars.contacts_xy_res:

                        # update its weight
                        if w > self.contacts[a, 3]:
                            self.contacts[a, 3] = w
                        # print "updated weight"

                        # we're done
                        continue

                # append if not close enough to any existing
                t = self.state.time_t
                id = self.state.get_id()
                row = np.concatenate((pos, [w], [t], [id]))
                self.contacts = np.append(self.contacts, row[np.newaxis, :], axis=0)
        # print "augmented", self.contacts.shape

        # output
        self.state.sig.contacted_list_WORLD = self.contacts

        # performance
        self.perf.tick_f()
