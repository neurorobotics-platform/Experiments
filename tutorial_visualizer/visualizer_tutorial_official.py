# ! /usr/bin/env python
from hbp_nrp_cle.brainsim import simulator as sim

# Some parameters
cellParams  = {}
cellType    = sim.IF_curr_alpha(**cellParams)

# Some neuron
someNeuron  = sim.Population(1, cellType, label='someNeuron')