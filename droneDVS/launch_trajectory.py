from geometry_msgs.msg import Twist

'''
Example of the way to control the quadrotor's motion.
Every 20 seconds the drone will go up and straight along the y axis, and then come back at its original location.
The drone should always start with a positive linear speed on the z-axis, simulating its take-off
'''


@nrp.Neuron2Robot(Topic('/quadrotor/cmd_vel', Twist))
def launch_trajectory(t):
    from geometry_msgs.msg import Twist, Vector3

    z_speed = 0.  # m/s
    y_speed = 0.
    angular_speed = 0.  # rad/s
    # going up to engage motors
    if t < 0.3:
        z_speed = 0.5  # m/s
        return Twist(linear=Vector3(x=0., y=y_speed, z=z_speed),
                     angular=Vector3(x=0., y=0., z=angular_speed))
    elif t < 0.4:
        z_speed = 0  # m/s
        return Twist(linear=Vector3(x=0., y=y_speed, z=z_speed),
                     angular=Vector3(x=0., y=0., z=angular_speed))
    return None
